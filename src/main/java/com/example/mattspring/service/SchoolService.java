package com.example.mattspring.service;

import com.example.mattspring.dto.SchoolDto;
import com.example.mattspring.exception.SchoolNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class SchoolService {
    List<SchoolDto> schoolList = List.of(SchoolDto.builder().id(UUID.randomUUID()).build(), SchoolDto.builder().id(UUID.randomUUID()).build());
    public List<SchoolDto> getAllSchools(){
        return schoolList;
    }
    public SchoolDto getSchoolById(UUID id){
        return schoolList.stream()
                .filter(schoolDto -> schoolDto.getId().equals(id))
                .findAny()
                .orElseThrow(SchoolNotFoundException::new);

    }
}
