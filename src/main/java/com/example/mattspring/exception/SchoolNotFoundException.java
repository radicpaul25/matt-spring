package com.example.mattspring.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class SchoolNotFoundException extends ResponseStatusException {
    public SchoolNotFoundException() {
        super(HttpStatus.NOT_FOUND, "School not found");
    }
}
