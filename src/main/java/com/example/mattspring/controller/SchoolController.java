package com.example.mattspring.controller;

import com.example.mattspring.dto.SchoolDto;
import com.example.mattspring.exception.SchoolNotFoundException;
import com.example.mattspring.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("school")
public class SchoolController {

    @Autowired
    SchoolService schoolService;

    @GetMapping()
    public List<SchoolDto> getAllSchools() {
        return schoolService.getAllSchools();
    }
    @GetMapping("/{id}")
    public SchoolDto getSchoolById(@PathVariable("id") UUID id) {
        return schoolService.getSchoolById(id);
    }
}
