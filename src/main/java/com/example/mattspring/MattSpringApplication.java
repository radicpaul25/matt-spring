package com.example.mattspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MattSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(MattSpringApplication.class, args);
    }
}
