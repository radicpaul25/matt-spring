package com.example.mattspring.dto;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;



@Data
@Builder
public class SchoolDto {
    private UUID id;
    private String name;
    private String postalCode;
}
