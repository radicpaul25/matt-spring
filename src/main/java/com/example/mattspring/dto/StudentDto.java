package com.example.mattspring.dto;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class StudentDto {
    private UUID id;
    private String firstName;
    private String lastName;
    private UUID schoolId;
}
